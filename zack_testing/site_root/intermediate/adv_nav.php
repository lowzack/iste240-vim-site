<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Advanced Navigation";
	$active = "home";
	$page1 = array(
	    "name" => "Movement",
	    "icon" => "fa fa-indent",
	    "this" => "#moving"
	);
	$page2 = array(
	    "name" => "Changing",
	    "icon" => "fa fa-list-ol",
	    "this" => "#change"
	);
	$page3 = array(
	    "name" => "Undo/Redo",
	    "icon" => "fa fa-check",
	    "this" => "#undoRedo"
	);
	$page4 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);


	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4
	);
	include (PATH_INC."functions.php");

	generatePage($page_title,$localNav,$navArrays,PATH_CON."intermediate/intermediate_navigation.html");
?>