<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Vim Tutorials";
	$active = "home";
	$page1 = array(
	    "name" => "Indentation",
	    "icon" => "fa fa-indent",
	    "this" => "#indent"
	);
	$page2 = array(
	    "name" => "Line Numbers",
	    "icon" => "fa fa-list-ol",
	    "this" => "#lineNum"
	);
	$page3 = array(
	    "name" => "Spell Check",
	    "icon" => "fa fa-check",
	    "this" => "#spell"
	);
	$page4 = array(
	    "name" => "Directories",
	    "icon" => "fa fa-folder-open",
	    "this" => "#directories"
	);
	$page5 = array(
	    "name" => "Tabs",
	    "icon" => "fa fa-folder",
	    "this" => "#tabs"
	);
	$page6 = array(
	    "name" => "Search",
	    "icon" => "fa fa-search",
	    "this" => "#search"
	);
	$page7 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);

	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4,
	    $page5,
	    $page6,
	    $page7
	);
	include (PATH_INC."functions.php");

	generatePage($page_title,$localNav,$navArrays,PATH_CON."intermediate/intermediate_workflow.html");
?>