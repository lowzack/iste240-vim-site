<?php
	session_start();
	include( "assets/inc/page_start.php");
	$page_title = "Quiz Grades";
	$active = "home";
	$localNav = array();
	include (PATH_INC."functions.php");

	include(PATH_INC."header.inc.php");

	echo "</div>
	<div class='col-md-9 col-sm-3'>";
	printLocalNav($localNav,"localNav");
	echo "</div>
	</div>

	<!-- Start Content -->
	<div class='row' id='p_holder'>
	        
	        <!-- Start Sidebar -->
	<div class='col-md-3 col-sm-3' id='p_holder'>";
	printNav($navArrays,"side","sideNav");
	echo "</div>
	<!-- End Sidebar -->
	<div class='col-md-9 col-sm-9'><div id='stickyAnchor'></div>";
	printLocalNav($localNav, "localNav");
	echo 
    "<div id='pageContent'>";?>

<h1><?= $_SESSION['username']?>'s Grades</h1>
<table class="table table-striped">
    <thead>
        <tr>
	        <th>Quiz</th><th>Score</th><th>Date</th><th>Missed Questions</th>
        </tr>
    </thead>
    <tbody>
<?php

  // GET THE COUNTRIE
  
  $query = "SELECT * FROM quiz_grades WHERE u_id = '".$_SESSION['u_id']."' ORDER BY date DESC";

  // Send query to DB
  $result = mysqli_query($link, $query);

// how many rows?
  $num_rows = mysqli_affected_rows($link);
  echo "<p>Number of Quizzes Taken: ".$num_rows."</p>";
  
  if ($result && $num_rows > 0 ){
    while ( $row = mysqli_fetch_assoc($result) ){
      echo "<tr><td>".$row['quiz_name']."</td><td>".
            $row['grade']."</td><td>".$row['date']. "</td><td>".$row['missed_questions']."</td></tr>";
    }
  }
  
?>

    </tbody>
</table>
<?php

echo 
    "            </div>

        </div>
        <!-- End Content -->
      </div>
      <!-- End Content -->

      <!-- Start Footer -->
      <div id='footer'>Copyright &copy; 2014<br>Made by the High Fives for RIT ISTE-240<br>Background image courtousy of <a href='http://subtlepatterns.com/grey-washed-wall/'>Subtle Patterns</a></div>
      <!-- End Footer -->

	    </div>
	  </body>
	</html>";?>