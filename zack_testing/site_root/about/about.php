<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Vim Tutorials";

	$page1 = array(
	    "name" => "Introduction",
	    "icon" => "fa fa-info-circle",
	    "this" => "#genIntro"
	);
	$page2 = array(
	    "name" => "History",
	    "icon" => "fa fa-history",
	    "this" => "#history"
	);
	$page3 = array(
	    "name" => "Timeline",
	    "icon" => "fa fa-clock-o",
	    "this" => "#devTime"
	);
	$page4 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);
	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4
	);
	include (PATH_INC."functions.php");

generatePage($page_title,$localNav,$navArrays,PATH_CON."about/about.html");
?>