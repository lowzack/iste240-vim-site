<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Quick Reference Table";
	$active = "home";
	$page1 = array(
	    "name" => "Exiting",
	    "icon" => "fa fa-close",
	    "this" => "#exit"
	);
	$page2 = array(
	    "name" => "Editing",
	    "icon" => "fa fa-pencil",
	    "this" => "#edit"
	);
	$page4 = array(
	    "name" => "Undo/Redo",
	    "icon" => "fa fa-undo",
	    "this" => "#undo"
	);
	$page5 = array(
	    "name" => "Moving",
	    "icon" => "fa fa-arrows",
	    "this" => "#tabs"
	);
	$page6 = array(
	    "name" => "Search",
	    "icon" => "fa fa-search",
	    "this" => "#search"
	);
	$page7 = array(
	    "name" => "Suspending",
	    "icon" => "fa fa-pause",
	    "this" => "#suspend"
	);
	$page8 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#search"
	);

	$localNav = array(
	    $page1,
	    $page2,
	    $page4,
	    $page5,
	    $page6,
	    $page7,
	    $page8
	);
	include (PATH_INC."functions.php");

	generatePage($page_title,$localNav,$navArrays,PATH_CON."reference/reference.html");
?>