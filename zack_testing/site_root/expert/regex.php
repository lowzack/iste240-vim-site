<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Regex";

	$page1 = array(
	    "name" => "Meta-Characters",
	    "icon" => "fa fa-home",
	    "this" => "#meta"
	);
	$page2 = array(
	    "name" => "Quantifiers",
	    "icon" => "fa fa-plus",
	    "this" => "#quantifiers"
	);
	$page3 = array(
	    "name" => "Character Ranges",
	    "icon" => "fa fa-font",
	    "this" => "#charRange"
	);
	$page4 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);

	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4
	);
	include (PATH_INC."functions.php");
	generatePage($page_title,$localNav,$navArrays,PATH_CON."expert/regex.html");
?>