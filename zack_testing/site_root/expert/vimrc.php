<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Vimrc";

	$page1 = array(
	    "name" => "Tabs",
	    "icon" => "fa fa-indent",
	    "this" => "#tabs"
	);
	$page2 = array(
	    "name" => "Movement",
	    "icon" => "fa fa-arrows",
	    "this" => "#movementBind"
	);
	$page3 = array(
	    "name" => "UI Config",
	    "icon" => "fa fa-columns",
	    "this" => "#uiConfig"
	);
	$page4 = array(
	    "name" => "Launch Config",
	    "icon" => "fa fa-rocket",
	    "this" => "#launchConfig"
	);
	$page5 = array(
	    "name" => "Backups",
	    "icon" => "fa fa-save",
	    "this" => "#backups"
	);
	$page6 = array(
	    "name" => "Color Scheme",
	    "icon" => "fa fa-paint-brush",
	    "this" => "#colors"
	);
	$page7 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);
	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4,
	    $page5,
	    $page6,
	    $page7
	);
	include (PATH_INC."functions.php");
	generatePage($page_title,$localNav,$navArrays,PATH_CON."expert/vimrc.html");
?>