<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Vim Tutorials";

	$page1 = array(
	    "name" => "Normal",
	    "icon" => "fa fa-terminal",
	    "this" => "#normal"
	);
	$page2 = array(
	    "name" => "Visual",
	    "icon" => "fa fa-eye",
	    "this" => "#visual"
	);
	$page3 = array(
	    "name" => "Select",
	    "icon" => "fa fa-sign-in",
	    "this" => "#select"
	);
	$page4 = array(
	    "name" => "Insert",
	    "icon" => "fa fa-plus",
	    "this" => "#insert"
	);
	$page5 = array(
	    "name" => "Replace",
	    "icon" => "fa fa-random",
	    "this" => "#replace"
	);
	$page6 = array(
	    "name" => "Command",
	    "icon" => "fa fa-terminal",
	    "this" => "#command"
	);
	$page7 = array(
	    "name" => "Ex",
	    "icon" => "fa fa-terminal",
	    "this" => "#ex"
	);
	$page8 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#command"
	);
	$localNav = array(
	    $page1,
	    $page2,
	    $page3,
	    $page4,
	    $page5,
	    $page6,
	    $page7,
	    $page8
	);
	include (PATH_INC."functions.php");
	generatePage($page_title,$localNav,$navArrays,PATH_CON."beginner/more_modes.html");
?>