<?php
	include( "../assets/inc/page_start.php");
	$page_title = "Vim Tutorials";

	$page1 = array(
	    "name" => "Basic Modes",
	    "icon" => "fa fa-home",
	    "this" => "#basicModes"
	);
	$page2 = array(
	    "name" => "Other Modes",
	    "icon" => "fa fa-home",
	    "this" => "#otherModes"
	);
	$page3 = array(
	    "name" => "Top of Page",
	    "icon" => "fa fa-angle-up",
	    "this" => "#top"
	);
	$localNav = array(
	    $page1,
	    $page2,
	    $page3
	);
	include (PATH_INC."functions.php");
	generatePage($page_title,$localNav,$navArrays,PATH_CON."beginner/modes.html");
?>