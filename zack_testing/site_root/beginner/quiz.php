<?php
	session_start();
	include("../assets/inc/page_start.php");
	include(PATH_INC."functions.php");
	if(!isset($_SESSION["u_id"])){
		$type = "error";
      $msg = "You must be logged in to take quizzes";
      $page_title = "Vim Tutorials";
      $active = "home";
      $localNav = array();
      $page = PATH_CON."home.html";
	    generatePage($page_title,$localNav,$navArrays,$page,$type,$msg);
	}else{

	$page_title = "Beginners Quiz!";
	$localNav = array();
	include(PATH_INC."header.inc.php");
	echo "</div>
	<div class='col-md-9 col-sm-3'>";
	printLocalNav($localNav,"localNav");
	echo "</div>
	</div>

	<!-- Start Content -->
	<div class='row' id='p_holder'>
	        
	        <!-- Start Sidebar -->
	<div class='col-md-3 col-sm-3'>
	<div id='stickyAnchor'></div>";
	printNav($navArrays,"side","sideNav");
	echo "</div>
	<!-- End Sidebar -->
	<div class='col-md-9 col-sm-9'>";
	printLocalNav($localNav, "localNav");
	echo 
    "<div id='pageContent'><div id='quiz'><h1>Beginner</h1><h1>Beginner Quiz</h1><p>The following quiz will assess your knowlege of the content in our About and Beginner sections of the site.</p>";
	$_SESSION['last_quiz'] = 'Beginner';

	// Get the Questions from the database
	
	$query = "SELECT * FROM b_questions ORDER BY RAND()";

	$result = mysqli_query($link, $query);

	$num_rows = mysqli_affected_rows($link);
	echo "<form id='b_quiz' action='".URL_INC."quizzer.php' method='post'>\n";
	if ($result && $num_rows > 0 ){
		$inc = 1;
		while ( $row = mysqli_fetch_assoc($result)){
			echo "<h2> Question #".$inc."</h2>\n";
			echo "<div id = 'question_".$row['q_id']."'><p>".$row['question']."</p>\n";
			echo "<ul>\n";
			$ans_query = "SELECT * FROM b_answers WHERE q_id = ".$row['q_id']." ORDER BY RAND()";
			$link_ans = $link;
			$ans_result = mysqli_query($link_ans, $ans_query);
			$ans_num_rows = mysqli_affected_rows($link_ans);
			if ($ans_result && $ans_num_rows > 0 ){
				while ( $ans_row = mysqli_fetch_assoc($ans_result)){
					echo "<input type='radio' id='answer_".$ans_row['a_id']."' value='answer_".$ans_row['a_id']."' name='question_".$row['q_id']."' required/>\n";
					echo "<label for= 'answer_".$ans_row['a_id']."'>".$ans_row['answer']."</label><br/>\n";
				}
			}
			echo "</ul>\n";
			echo "</div>";
			$inc += 1;
		}
	}
	echo "<input type='submit' value='Submit'/>";
	echo "</form>\n";

echo 
    "</div></div>
        <!-- End Content -->
      </div>
      <!-- End Content -->

      <!-- Start Footer -->
      <div id='footer'>Copyright &copy; 2014<br>Background image courtousy of <a href='http://subtlepatterns.com/grey-washed-wall/'>Subtle Patterns</a></div>
      <!-- End Footer -->

	    </div>
	  </body>
	</html>";}
	?>