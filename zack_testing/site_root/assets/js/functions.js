function validateRegister(){
	document.getElementById('errorMsg').innerHTML = "";
	$('#errorBox').slideUp();
	
	if(
		document.getElementById('r_username').value.length < 4 ){
		document.getElementById('errorMsg').innerHTML = "Username must be 4 characters or longer";
		$('#errorBox').slideDown();
		return false;
	}
	else if(document.getElementById('r_password').value.length < 8 ||
		document.getElementById('r_password').value != document.getElementById('r_cPassword').value){
		document.getElementById('errorMsg').innerHTML = "Passwords don't match or are less than 8 characters";
		$('#errorBox').slideDown();
		return false;
	}
}