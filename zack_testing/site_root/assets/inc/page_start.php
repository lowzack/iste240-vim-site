<?php
  // Initialize everything I'll need for the page
  // /Volumes/Students/students/141/HighFives/Sites/vim
  
  define( "PATH", "/Volumes/Students/students/141/HighFives/Sites/site_root/" );
  define( "PATH_INC", PATH . "assets/inc/" );
  
  define( "URL", "http://nova.it.rit.edu/~highfives/site_root/" );
  define( "URL_CSS", URL . "assets/css/" );
  define( "URL_JS", URL . "assets/js/" );
  define( "URL_MEDIA", URL . "assets/media/");
  define( "URL_INC", URL . "assets/inc/" );
  define( "PATH_CON", PATH . "assets/content/");
  
  define( "SITE_TITLE", "Vim & Vigor" );
  
  
  // Include any PHP function libraries or classes
  include "/Volumes/Students/students/141/HighFives/Sites/db_conn.php";

  // Initialize the database connection
  $link = mysqli_connect( $db_host, $db_user, $db_pass, $db_name);

  // Verify valid connection
  if (!$link){
    echo "connection error: " . mysqli_connect_error();
    die();
  }
  
?>