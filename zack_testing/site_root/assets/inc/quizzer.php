<?php
	session_start();

	include("page_start.php");
	include(PATH_INC."functions.php");
	$num_ques = 0;
	$num_correct = 0;
	$questions_missed = "";

	$a_table = "_answers";
	$q_table = "_questions";

	if ($_SESSION['last_quiz'] == "Beginner" ){
		$a_table = "b".$a_table;
		$q_table = "b".$q_table;
	}else if ($_SESSION['last_quiz'] == "Intermediate"){
		$a_table = "i".$a_table;
		$q_table = "i".$q_table;
	}

	foreach($_POST as $key => $value){
		$num_ques += 1;

		$ques = (int) preg_replace('/\D/', '', $key);
		$ans = (int) preg_replace('/\D/', '', $value)."<br/>";
		$new_q = "SELECT * FROM `".$a_table."` JOIN `".$q_table."` ON `".$a_table."`.`q_id` WHERE `".$a_table."`.`q_id`=`".$q_table."`.`q_id` AND `".$a_table."`.`q_id`=".$ques." AND `correct` = 1";
		$val_query = "SELECT * FROM ".$a_table." WHERE q_id = ".$ques." AND correct = 1";
		$link_quer = $link;
		$ans_result = mysqli_query($link_quer, $new_q);
		$val_num_rows = mysqli_affected_rows($link_quer);
		if ($ans_result && $val_num_rows > 0){
			while ( $row = mysqli_fetch_assoc($ans_result)){
				if ((int)$row['a_id'] == $ans){
					$num_correct += 1;
					
				}else{
					$questions_missed .= "".$row['question']."<br/>";
				}
			}
		}
		/*
		if ($ans_result && $ans_num_rows > 0 ){
			while ( $ans_row = mysqli_fetch_assoc($ans_result)){
				echo "<input type='radio' id='answer_".$ans_row['a_id']."' value='answer_".$ans_row['a_id']."' name='question_".$row['q_id']."' />\n";
				echo "<label for= 'answer_".$ans_row['a_id']."'>".$ans_row['a_id'].": ".$ans_row['answer']."</label><br/>\n";
			}
		}*/
	}
	$grade = ($num_correct/$num_ques)*100;
	if(isset($_SESSION['u_id'])){
		$query = "INSERT INTO quiz_grades (`id`, `quiz_name`, `grade`,`date`, `u_id`, `missed_questions`)
		 		  VALUES (NULL, '".$_SESSION['last_quiz']."', '".$grade."','".date("m/d/Y H:i")."','".$_SESSION['u_id']."','".$questions_missed."');";
		$result = mysqli_query($link, $query);
		$val_num_rows = mysqli_affected_rows($link);
		if ($result && $val_num_rows > 0){

		}else{

		}
	}
	header("Location: ".URL."grades.php");
	die();
?>