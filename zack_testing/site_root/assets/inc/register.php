<?php
	include( "page_start.php");
	include(PATH_INC."functions.php");

	if ( isset($_POST['submit']) && $_POST['r_password'] == $_POST['r_cPassword'] ){
	    $query = "
	              INSERT INTO users SET username='".sanitize($_POST['r_username'])."',
	              password = '".SHA1($_POST['r_password'])."', email = '".sanitize($_POST['r_email'])."'";
	    $result = mysqli_query($link,$query);

	    $num_rows = mysqli_affected_rows($link);

	    if ($result && $num_rows > 0){
	      $type = "success";
	      $msg = "Successfully registerd as ".$_POST['r_username'].". Please log in!";
	      $page_title = "Vim Tutorials";
	      $active = "home";
	      $localNav = array();
	      $page = PATH_CON."home.html";
	    } else{
	      $type = "error";
	      $msg = "Username already in use. Please pick a new username and try again.";
	      $page_title = "Register";
	      $active = "home";
	      $localNav = array();
	      $page = PATH_CON."register.html";
	    }
	    generatePage($page_title,$localNav,$navArrays,$page,$type,$msg);
	}else{
		$type = "error";
	    $msg = "Password mismatch, please try again.";
	    $page_title = "Register";
	    $active = "home";
	    $localNav = array();
	    $page = PATH_CON."register.html";
	    generatePage($page_title,$localNav,$navArrays,$page,$type,$msg);
	}
?>