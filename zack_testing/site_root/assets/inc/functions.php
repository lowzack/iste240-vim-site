<?php
$home = array(
	"name" => "Home",
	"icon" => "fa fa-home",
	"current" => false,
	"this" => URL."index.php"
);
$about = array(
	"name" => "About",
	"icon" => "fa fa-info-circle",
	"current" => false,
	"General Information" => URL."about/about.php#generalinfo",
	"History" => URL."about/about.php#history",
	"Development Timeline" => URL."about/about.php#devTime"
);
$beginners = array(
	"name" => "Beginner",
	"icon" => "fa fa-graduation-cap",
	"current" => false,
	"Installation" => URL."beginner/installation.php",
	"Vim's Modes" => URL."beginner/modes.php",
	"Modes Continued" => URL."beginner/modes_cont.php",
	"Navigating Vim" => URL."beginner/navigation.php",
	"Beginner Quiz" => URL."beginner/quiz.php"
);
$intermediate = array(
	"name" => "Intermediate",
	"icon" => "fa fa-graduation-cap",
	"current" => false,
	"Workflow" => URL."intermediate/workflow.php",
	"Whitespace" => URL."intermediate/whitespace.php",
	"Advanced Navigation" => URL."intermediate/adv_nav.php",
	"Intermediate Quiz" => URL."intermediate/quiz.php"
);
$expert = array(
	"name" => "Expert",
	"icon" => "fa fa-graduation-cap",
	"current" => false,
	"Plugins" => URL."expert/plugins.php",
	"REGEX" => URL."expert/regex.php",
	"Vimrc" => URL."expert/vimrc.php"
);
$reference = array(
	"name" => "Reference",
	"icon" => "fa fa-book",
	"current" => false,
	"this" => URL."reference/reference.php"
);
$navArrays = array(
	$home,
	$about,
	$beginners,
	$intermediate,
	$expert,
	$reference
);
function sanitize( $data ){
      $data = addslashes( $data );
      $data = trim( $data );
      $data = strip_tags( $data );
      $data = htmlentities( $data );
      
      return $data;
  }

function printNav($arrays,$id,$class){
	$home = "index.php";

	$nav = "<div id='$id' class='$class' ><nav><ul>";

	// For each array in $arrays
	foreach ($arrays as $curr) {
		// Get the name and generate the first level
		// for each array
		if(count($curr) <= 4 && isset($curr["this"])){
			$nav .= "<li><a href='".$curr["this"]."'><h3><i class='".$curr["icon"]."'></i>".$curr['name']."</h3></a>\n";
		}else{
			if ($curr["current"] == true){
				$nav .= "<li class='active'><h3><i class='".$curr["icon"]."'></i>".$curr['name']."</h3>\n";
			}else{
				$nav .= "<li><h3><i class='".$curr["icon"]."'></i>".$curr['name']."</h3>\n";
			}
		    
			$nav .= "<ul>\n";
			foreach ($curr as $key => $val) {
				if($key == "name" || $key == "icon" || $key == "current"){
					
				}else{
					$nav .= "<li><a href='$val'><i class='fa fa-circle-o'></i>$key</a></li>\n";
				}
			}
			$nav .= "</ul>\n";
		}
		$nav .="</li>\n";
	}
	$nav .= "</ul></nav></div>\n";
	echo "$nav";
}

function printLocalNav($arrays,$class,$id='navbar'){
    $home = "index.php";
    if(count($arrays)!=0){
	    $nav = "<div class='".$class."'><nav class='navbar navbar-default' role='navigation'>
	        <div class='container-fluid'>
	          <div class='navbar-header'>
	            <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#".$id."' aria-expanded='false' aria-controls='navbar'>
	              <span class='sr-only'>Toggle navigation</span>
	              <span class='icon-bar'></span>
	              <span class='icon-bar'></span>
	              <span class='icon-bar'></span>
	            </button>
	            <a class='navbar-brand' href='#'>Page Navigation</a>
	          </div>
	          <div id='".$id."' class='navbar-collapse collapse'>
	            <ul class='nav navbar-nav'>";
	    foreach ($arrays as $curr) {
	        // Get the name and generate the first level
	        // for each array

	        if(count($curr) <= 3 && isset($curr["this"])){
	            $nav .= "<li><a href='".$curr["this"]."'><i class='".$curr["icon"]."'></i>".$curr['name']."</a>\n";
	        }else{
	            
	        }
	        $nav .="</li>\n";
	    }
	    $nav .= "</ul></div></div></nav></div>\n";
	    echo "$nav";
	}
}

function printContent($page,$mType,$msg){
    echo 
    "<div id='pageContent'>";
    if ($mType == "success"){
    	echo "<div class='alert alert-success'>$msg</div>";
    }else if($mType == "error"){
    	echo "<div class='alert alert-danger'>$msg</div>";
    }
    include $page;
    echo 
    "            </div>

        </div>
        <!-- End Content -->
      </div>
      <!-- End Content -->
      <div class='clearfix'></div>
      <!-- Start Footer -->
      <div id='footer'>Copyright &copy; 2014<br>Made by the High Fives for RIT ISTE-240<br>Background image courtousy of <a href='http://subtlepatterns.com/grey-washed-wall/'>Subtle Patterns</a></div>
      <!-- End Footer -->

    </div>
  </body>
</html>";
    
}

function generatePage($title,$localnav,$globalNav, $content, $mType="none", $msg=""){
	$page_title = $title;
	include(PATH_INC."header.inc.php");

	// printNav($globalNav,"f_side","nav");
	echo "</div>
	<div class='col-md-9 col-sm-3'>";
	printLocalNav($localnav,"localNav");
	echo "</div>
</div>

<!-- Start Content -->
<div class='row' id='p_holder'>
        
        <!-- Start Sidebar -->
<div class='col-md-3 col-sm-3'>
";
	printNav($globalNav,"side","sideNav");
	echo "</div>
<!-- End Sidebar -->
<div class='col-md-9 col-sm-9'><div id='stickyAnchor'></div>";
	printLocalNav($localnav, "localNav","navbar2");
	printContent($content,$mType,$msg);
}

?>