<?php session_start(); ?>
<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content=''>
    <meta name='author' content=''>

    <title>Vim and Vigor - <?= $page_title ?></title>

    <link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'>
  	<link rel='stylesheet' type='text/css' href='<?= URL_CSS ?>styles.css'/>

  	<script type='text/javascript' src='http://code.jquery.com/jquery-1.9.1.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js'></script>
    <script type="text/javascript" src='<?= URL_JS ?>functions.js'></script>
    <script type="text/javascript" src='<?= URL_JS ?>smooth-scroll.js'></script>


    <script src='<?= URL_JS ?>sticky.js'></script>
    <script src='<?= URL_JS ?>navigation.js'></script>

  </head>

  <body>
  <a name="top"></a>
    <div id='siteHolder'>

      <!-- Start Header -->
      <div id='header' class='row'>
  			<div id='leftHeader' class='col-md-9'>
  				<img id='logo' src='<?= URL_MEDIA ?>images/logo2.png' alt='logo'/>
  			</div>
  			<div id='rightHeader' class='col-md-3'>
  				<div class='floatRight'>
  					<?php 

              if(isset($_SESSION['username'])){
                echo "Welcome, ".$_SESSION['username']."<br/>";
                echo "<a href='".URL."grades.php'>Quiz Grades</a><br/>";
                echo "<a href='".URL_INC."logout.php'>Log Out</a>";
              }else{
                include PATH_INC.'login.inc.php';
              }

            ?>
  				</div>
  			</div>
		  </div>
      <!-- End Header -->
      <div class='imFixed row'>
        <div class='col-md-3 col-sm-3'>